import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StarComponent} from './star/star.component';
import {FormsModule} from '@angular/forms';
import { CollapsibleProductComponent } from './collapsible-product.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    StarComponent,
    CollapsibleProductComponent
  ],
  exports: [
    StarComponent,
    CollapsibleProductComponent,
    CommonModule,
    FormsModule
  ]
})
export class SharedModule { }
