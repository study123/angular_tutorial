import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'collapsible-product',
  templateUrl: './collapsible-product.component.html',
  styleUrls: ['./collapsible-product.component.css']
})
export class CollapsibleProductComponent implements OnInit {

  visible = true;

  constructor() { }

  ngOnInit() {
  }

  toggleContent(): void {
    this.visible = !this.visible;
  }

  showHide(): string {
    return this.visible ? 'fa-minus' : 'fa-plus';
  }

}
