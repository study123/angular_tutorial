import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import {RouterModule} from '@angular/router';
import { NotfoundComponent } from './notfound/notfound.component';
import {ProductModule} from './products/product.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotfoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ProductModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent},
      { path: 'products', loadChildren: 'app/products/product.module#ProductModule'},
      { path: 'user', loadChildren: 'app/user/user.module#UserModule'},
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: '**', component: NotfoundComponent},
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
