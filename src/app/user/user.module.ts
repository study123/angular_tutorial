import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: 'profile', component: UserProfileComponent}
    ])
  ],
  declarations: [UserProfileComponent]
})
export class UserModule { }
