import { Component, OnInit } from '@angular/core';
import {ProductModel} from './product.model';
import {ProductService} from './product.service';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {IProduct} from './product';


@Component({
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  private pageTitle = 'Create Product';
  private _product: IProduct;
  private productCodes = ['asdfa-sd', '23aas4-asa', 'AB-344', 'ddd-222'];
  private _hasProductCodeError = false;
  private _hasPriceError = false;

  public isDirty = false;

  constructor(private _productService: ProductService, private _route: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._product = new ProductModel();
  }

  productNameToUpperCase(value: string): void {
    if (!this._product.productName) {
      this._product.productName = '';
    }
    if (value.length > 0) {
      this._product.productName = value.charAt(0).toUpperCase() + value.slice(1);
    } else {
      this._product.productName = value;
    }
  }

  validateProductCode(value: string): void {
    if (value === '' || value === 'default') {
      this._hasProductCodeError = true;
    } else {
      this._hasProductCodeError = false;
    }
  }

  validatePrice(value: string): void {
    if (isNaN(parseFloat(value))) {
      this._hasPriceError = true;
    } else {
      this._hasPriceError = false;
    }
  }

  submitForm(product: IProduct): void {
    this._productService.postProduct(product)
      .subscribe(
        data => {
          this._router.navigate(['/products/list']);
        },
        err => console.log('error:', err)
      );
  }

  formChange(dirty: boolean): void {
    this.isDirty = dirty;
  }
}
