import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ProductService} from './product.service';
import {IProduct} from './product';

@Injectable()
export class ProductResolverService implements Resolve<IProduct>{

  constructor(private productService: ProductService) { }

  resolve(route: ActivatedRouteSnapshot) {
    const id = +route.paramMap.get('id');
    return this.productService.getProduct(id).map((product) => {
      return product;
    });
  }
}
