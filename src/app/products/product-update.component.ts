import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IProduct} from './product';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductService} from './product.service';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {
  private _product: IProduct;
  public pageTitle = 'Update: ';

  productName: FormControl;
  productPrice: FormControl;

  productForm: FormGroup;

  constructor(private _route: ActivatedRoute, private productService: ProductService, private _router: Router) { }

  ngOnInit() {
    this._product = this._route.snapshot.data['product'];
    let productId = new FormControl(this._product.productId)
    this.productName = new FormControl(this._product.productName,
      [
        Validators.required,
        Validators.pattern('...+'),
        this.restrictedWords
        // this.restrictedWords(['foo', 'bar']) - not working
      ])

    this.productPrice = new FormControl(this._product.price, Validators.required)
    this.productForm = new FormGroup({
      productId: productId,
      productName: this.productName,
      price: this.productPrice
    })
  }

  // Not working
  // private restrictedWords(words: []) {
  //   return (control: FormControl): {[key: string]: any} => {
  //     if (!words) return null;
  //
  //     var invalidWords = words
  //       .map(w => control.value.includes(w) ? w: null)
  //       .filter(w => w != null)
  //
  //     return invalidWords && invalidWords.length > 0
  //       ? {'restrictedWords': invalidWords.join(', ')}
  //       : null;
  //   }
  // }

  private restrictedWords(control: FormControl): {[key: string]: any} {
    return control.value.includes('foo') ? {'restrictedWords': 'foo'} : null;
  }

  saveProduct(product: IProduct): void {
    if (this.productForm.valid) {
      this.productService.postProduct(product)
        .subscribe(
          data => {
            this._router.navigate(['/products/list']);
          },
          err => console.log('error:', err)
        );
    }
  }

  validateProductName(): boolean {
    return this.productName.valid
  }

}
