import {IProduct} from './product';

export class ProductModel implements IProduct {
  public productId: number;
  public productName: string;
  public productCode: string;
  public releaseDate: string;
  public price: number;
  public description: string;
  public starRating: number;
  public imageUrl: string;
  public available: boolean;
  constructor() {}
}
