import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductListComponent} from './productList.component';
import {ProductDetailComponent} from './product-detail.component';
import {ConvertToSpacesPipe} from '../shared/convert-to-spaces.pipe';
import {RouterModule} from '@angular/router';
import {ProductGuardService} from './product-guard.service';
import {ProductService} from './product.service';
import { SharedModule } from '../shared/shared.module';
import { ProductCreateComponent } from './product-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ProductUpdateComponent } from './product-update.component';
import {ProductResolverService} from './product-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: 'list', component: ProductListComponent},
      { path: 'create', component: ProductCreateComponent, canDeactivate: ['canDeactivateCreateEvent']},
      { path: 'update/:id', component: ProductUpdateComponent, resolve: {
        product: ProductResolverService
      }},
      { path: 'view/:id', canActivate: [ProductGuardService], component: ProductDetailComponent},
    ]),
    SharedModule,
  ],
  declarations: [
    ProductListComponent,
    ProductDetailComponent,
    ConvertToSpacesPipe,
    ProductCreateComponent,
    ProductUpdateComponent,
  ],
  providers: [
    ProductService,
    ProductGuardService,
    ProductResolverService,
    {
      provide: 'canDeactivateCreateEvent',
      useValue: checkDirtyState
    }
  ]
})
export class ProductModule { }


function checkDirtyState(component: ProductCreateComponent) {
  if (component.isDirty) {
    return window.confirm('Exit?');
  }
  return true;
}
