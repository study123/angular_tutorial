import {Injectable} from '@angular/core';
import {IProduct} from './product';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {ProductModel} from './product.model';

@Injectable()

export class ProductService {
  private _productUrl = 'http://localhost:3100/getProducts';
  private _postUrl = 'http://localhost:3100/postProducts';

  constructor(private _http: HttpClient) {}

  getProducts(): Observable<IProduct[]> {
    return this._http.get<IProduct[]>(this._productUrl)
      .do(data => console.log('All: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  getProduct(productId: number): Observable<IProduct> {
    return this._http.get<IProduct[]>(this._productUrl)
      .map(data => {
        let product = data.find(product => product.productId === productId);
        return product;
      })
      .do(data => console.log('All: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  postProduct(product: IProduct): Observable<any> {
    console.log('posting product');

    const body = JSON.stringify(product);
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this._http.post(this._postUrl, body, {headers: headers})
      .map(this.extractData)
      .catch(this.handleError);
  }

  extractData(res: HttpResponse<any>) {
    console.log(res);
    return res || {};
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err);
    return Observable.throw(err.message);
  }
}
