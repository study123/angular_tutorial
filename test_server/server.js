var http = require('http');
var formidable = require('formidable');
var util = require('util');

var products = [
  {
    "productId": 1,
    "productName": "Leaf Rake",
    "productCode": "GDN-0011",
    "releaseDate": "March 19, 2016",
    "description": "Leaf rake with 48-inch wooden handle.",
    "price": 19.95,
    "starRating": 3.2,
    "imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
  },
  {
    "productId": 2,
    "productName": "Garden Cart",
    "productCode": "GDN-0023",
    "releaseDate": "March 18, 2016",
    "description": "15 gallon capacity rolling garden cart",
    "price": 32.99,
    "starRating": 4.2,
    "imageUrl": "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
  },
  {
    "productId": 3,
    "productName": "Hammer",
    "productCode": "TBX-0048",
    "releaseDate": "May 21, 2016",
    "description": "Curved claw steel hammer",
    "price": 8.9,
    "starRating": 4.8,
    "imageUrl": "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
  },
  {
    "productId": 4,
    "productName": "Saw",
    "productCode": "TBX-0022",
    "releaseDate": "May 15, 2016",
    "description": "15-inch steel blade hand saw",
    "price": 11.55,
    "starRating": 3.7,
    "imageUrl": "http://openclipart.org/image/300px/svg_to_png/27070/egore911_saw.png"
  },
  {
    "productId": 5,
    "productName": "Video Game Controller",
    "productCode": "GMG-0042",
    "releaseDate": "October 15, 2015",
    "description": "Standard two-button video game controller",
    "price": 35.95,
    "starRating": 4.6,
    "imageUrl": "http://openclipart.org/image/300px/svg_to_png/120337/xbox-controller_01.png"
  }
]


var server = http.createServer(function(req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Request-With, Content-Type, Accept-Header');

  if (req.method.toLowerCase() == 'post') {
    processForm(req ,res);
    return;
  }

  if (req.method.toLowerCase() == 'get') {
    res.writeHead(200, {
      'content-type': 'application/json'
    })

    res.end(JSON.stringify(products));
    return ;
  }

  res.end();

})

function processForm(req, res){
  var form = new formidable.IncomingForm();

  form.parse(req, function(err, fields) {

    console.log(fields);

    if (fields.productId) {
      var index = fields.productId-1;
      products[index].productName = fields.productName
      products[index].price = fields.price
    } else {
      fields['productId'] = products.length + 1;
      products.push(fields)
    }


    res.writeHead(200, {
      'content-type': 'application/json'
    })

    var data = JSON.stringify(
      fields
    );

    res.end(data);

    console.log('posted fields \n')
    console.log(data)
  })
}

var port = 3100;
server.listen(port);

console.log('Server started \n')
